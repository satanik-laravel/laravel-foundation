<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 26.03.18
 * Time: 13:02
 */

namespace Satanik\Foundation\Tests;

use Artisan;
use Hash;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Testing\{RefreshDatabase, TestCase as BaseTestCase};
use Satanik\Foundation\Abstraction\User;

abstract class TestCase extends BaseTestCase
{
    use RefreshDatabase;

    protected static $seeded = false;

    /** @var bool $use_user Set to false if no user is being used in the current tests */
    protected $use_user = true;

    /** @var \App\User $user */
    protected $user;
    protected $user_token;

    public static function seeded(): bool
    {
        return self::$seeded;
    }

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication(): Application
    {
        $app = require __DIR__ . '../../../../../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        Hash::setRounds(4);

        return $app;
    }

    /**
     * Setup the test environment.
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->seedApp();

        $this->initialiseUser();
    }

    protected function seedApp(): void {
        if (!self::$seeded) {
            Artisan::call('db:seed', [ '--no-interaction' => true ]);
            self::$seeded = true;
        }
    }

    protected function initialiseUser(): void
    {
        if ($this->use_user) {
            $this->user       = factory(User::bound())->create();
            $this->user_token = $this->user->token;
        }
    }
}
