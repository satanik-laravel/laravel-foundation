<?php

namespace Satanik\Foundation\Tests;

require_once __DIR__ . '/ExampleEnum.php';

class EnumTest extends TestCase
{
    protected $use_database = false;
    protected $use_user     = false;

    /**
     * @throws \ReflectionException
     */
    public function testAccessEnumValues(): void
    {
        $this->assertEquals(['FOOBAR' => 0, 'BAZBAR' => 1], ExampleEnum::make()->options());
    }
}
