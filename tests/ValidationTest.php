<?php

namespace Satanik\Foundation\Tests;

use Validator;

class ValidationTest extends TestCase
{
    public function testForbiddenWithAny(): void
    {
        $rules = ['optionA' => 'forbidden_with_any:optionB,optionC'];
        $v     = Validator::make([
            'optionA' => true,
        ], $rules);

        $this->assertFalse($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true
        ], $rules);

        $this->assertTrue($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionC' => true
        ], $rules);

        $this->assertTrue($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true,
            'optionC' => true
        ], $rules);

        $this->assertTrue($v->fails());
    }

    public function testForbiddenWithAll(): void
    {
        $rules = ['optionA' => 'forbidden_with_all:optionB,optionC'];
        $v     = Validator::make([
            'optionA' => true,
        ], $rules);

        $this->assertFalse($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true
        ], $rules);

        $this->assertFalse($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionC' => true
        ], $rules);

        $this->assertFalse($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true,
            'optionC' => true
        ], $rules);

        $this->assertTrue($v->fails());
    }

    public function testForbiddenWithoutAny(): void
    {
        $rules = ['optionA' => 'forbidden_without_any:optionB,optionC'];
        $v     = Validator::make([
            'optionA' => true,
        ], $rules);

        $this->assertTrue($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true
        ], $rules);

        $this->assertFalse($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionC' => true
        ], $rules);

        $this->assertFalse($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true,
            'optionC' => true
        ], $rules);

        $this->assertFalse($v->fails());
    }

    public function testForbiddenWithoutAll(): void
    {
        $rules = ['optionA' => 'forbidden_without_all:optionB,optionC'];
        $v     = Validator::make([
            'optionA' => true,
        ], $rules);

        $this->assertTrue($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true
        ], $rules);

        $this->assertTrue($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionC' => true
        ], $rules);

        $this->assertTrue($v->fails());

        $v = Validator::make([
            'optionA' => true,
            'optionB' => true,
            'optionC' => true
        ], $rules);

        $this->assertFalse($v->fails());
    }
}
