<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 13.03.18
 * Time: 12:26
 */


namespace Satanik\Foundation\Tests;

use Satanik\Foundation\Abstraction\Enum;

class ExampleEnum extends Enum
{
    public const FOOBAR = 0b0;
    public const BAZBAR = 0b1;
}
