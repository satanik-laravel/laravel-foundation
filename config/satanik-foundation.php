<?php

use Satanik\Foundation\Abstraction\User;
use Satanik\Foundation\Abstraction\UserResource;

return [
    'user_model'    => User::class,
    'user_resource' => UserResource::class,
];
