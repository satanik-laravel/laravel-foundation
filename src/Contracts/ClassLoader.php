<?php

namespace Satanik\Foundation\Contracts;

use Illuminate\Support\Collection;

interface ClassLoader
{
    public static function implements(string $interface): Collection;

    public static function inherits(string $parent): Collection;
}
