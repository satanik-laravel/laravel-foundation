<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 14.11.17
 * Time: 17:04
 */

namespace Satanik\Foundation\Concerns;

use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Satanik\Exceptions\Types\Error;
use Schema;

/** @noinspection PhpUndefinedClassInspection */

/**
 * Trait ModelTrait
 * @package App\Traits
 * ** scope methods
 * @method static self nPerGroup(string $group, int $n = 10)
 * @method static self joinModel(string $model, string $one, string $operator = null, string $two = null, string $type = 'inner', bool $where = false)
 * ** Builder methods
 * @method static self create(array $attributes = [])
 * @method static self forceCreate(array $attributes)
 * @method static self update(array $values)
 * @method static self joinPivot(string $table)
 * @method static self join(string $table, string $one, string $operator = null, string $two = null, string $type = 'inner', bool $where = false)
 * @method static self from(string $table)
 * @method static self find(int $id)
 * @method static self findOrFail($id, $columns = ['*'])
 * @method static self first(array $columns = ['*'])
 * @method static self firstOrCreate(array $attributes, array $values = [])
 * @method static self where(string | array | \Closure $column, string $operator = null, $value = null, string $boolean = 'and')
 * @method static self whereIn(string $column, array | mixed $values, string $boolean = 'and', bool $not = false)
 * @method static self whereNotIn(string $column, mixed $values, string $boolean = 'and')
 * @method static self whereNotNull(string $column, string $boolean = 'and')
 * @method static self whereRaw(string $sql, mixed $bindings = [], string $boolean = 'and')
 * @method static self leftJoin(string $table, string $first, string $operator = null, string $second = null)
 * @method static self orderBy(string $column, string $direction = 'asc')
 * @method static self select(array | string... $columns = ['*'])
 * @method static self with(array | string... $relations)
 * @method static self withTrashed()
 * @method static self withCount(array | string... $relations)
 * @method static self whereHas(string $relation, \Closure | null $callback = null, string $operator = '>=', int $count = 1)
 * @method static self truncate()
 * @method static self insert(array $values)
 * @method static int|float max(string $column)
 * @method static bool exists()
 * @method self|Collection|HasMany hasMany(string $related, string $foreignKey = null, string $localKey = null)
 * @method self|Collection|BelongsTo belongsTo(string $related, string $foreignKey = null, string $otherKey = null, string $relation = null)
 * @method self|Collection|BelongsToMany belongsToMany(string $related, string $table = null, string $foreignKey = null, string $otherKey = null, string $relation = null)
 * @method self|Collection|BelongsToMany withTimestamps(mixed $createdAt = null, mixed $updatedAt = null)
 * @method static \Illuminate\Database\Query\Builder newQuery()
 * @mixin \Eloquent
 *  ** mix them in as instance methods
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Illuminate\Database\Query\Builder
 *   ** mix them in as static method calls
 * @mixin static \Illuminate\Database\Eloquent\Builder
 * @mixin static \Illuminate\Database\Query\Builder
 */
trait IsModel
{
    public static function getTableName(): string
    {
        return with(new static)->getTable();
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string                                $model
     * @param string                                $one
     * @param string|null                           $operator
     * @param string|null                           $two
     * @param string                                $type
     * @param bool                                  $where
     *
     * @throws \Satanik\Exceptions\Types\Exception
     * @throws \Satanik\Exceptions\Types\ValidationException
     */
    public function scopeJoinModel(
        Builder $query,
        string $model,
        string $one,
        string $operator = null,
        string $two = null,
        string $type = 'inner',
        bool $where = false
    ): void {
        \Assert::uses($model, IsModel::class);

        /** @var IsModel $model */

        $query->join($model::getTableName(), $one, $operator, $two, $type, $where);
    }

    /**
     * @param Builder $query
     * @param string  $table
     *
     * @return Builder mixed
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function scopeJoinPivot(Builder $query, string $table): Builder
    {
        \Assert::exception(Schema::hasTable($table), Error::TABLE_DOES_NOT_EXIST, [
            'key'  => 'table',
            'data' => $table,
        ]);

        $ours   = str_plural($this->getTable());
        $our    = str_singular($ours);
        $theirs = str_plural($table);
        $their  = str_singular($theirs);
        if (Schema::hasTable($ours . '_' . $theirs)) {
            {
                $pivot = $ours . '_' . $theirs;
            }
        } elseif (Schema::hasTable($ours . '_' . $their)) {
            {
                $pivot = $ours . '_' . $their;
            }
        } elseif (Schema::hasTable($our . '_' . $theirs)) {
            {
                $pivot = $our . '_' . $theirs;
            }
        } elseif (Schema::hasTable($our . '_' . $their)) {
            {
                $pivot = $our . '_' . $their;
            }
        }

        /** @var string $pivot */
        \Assert::exception($pivot !== null, Error::TABLE_NOT_DEDUCABLE, [
            'table to join from'      => $this->getTable(),
            'table to join via pivot' => $table,
        ]);

        if (Schema::hasColumn($pivot, $ours . '_id')) {
            {
                $ours_id = $pivot . '.' . $ours . '_id';
            }
        } elseif (Schema::hasColumn($pivot, $our . '_id')) {
            {
                $ours_id = $pivot . '.' . $our . '_id';
            }
        }

        if (Schema::hasColumn($pivot, $table . '_id')) {
            {
                $their_id = $pivot . '.' . $table . '_id';
            }
        } elseif (Schema::hasColumn($pivot, $their . '_id')) {
            {
                $their_id = $pivot . '.' . $their . '_id';
            }
        }

        \Assert::exception(!empty($ours_id) && !empty($their_id), Error::TABLE_DOES_NOT_EXIST, [
            'key'  => 'deduced columns',
            'data' => [$ours_id, $their_id],
        ]);

        /** @var string $their_id */
        return $query->join($pivot, $ours . '.id', $ours_id)
                     ->join($table, $table . '.id', $their_id);
    }

    /**
     * @param Builder $query
     * @param string  $group
     * @param int     $number
     *
     * @return void
     */
    public function scopeNPerGroup(Builder $query, string $group, int $number = 10): void
    {
        /** @var EloquentModel $this */
        // queried table
        $table = $this->getTable();

        // initialize MySQL variables inline
        $query->from(DB::raw("(SELECT @rank:=0, @group:=0) as vars, {$table}"));

        // if no columns already selected, let's select *
        if (!$query->getQuery()->columns) {
            $query->select("{$table}.*");
        }

        // make sure column aliases are unique
        $group_alias = 'group_' . md5(time());
        $rank_alias  = 'rank_' . md5(time());

        // apply mysql variables
        $query->addSelect(DB::raw(
            "@rank := IF(@group = {$group}, @rank+1, 1) as {$rank_alias}, @group := {$group} as {$group_alias}"
        ));

        // make sure first order clause is the group order
        $query->getQuery()->orders = (array)$query->getQuery()->orders;
        array_unshift($query->getQuery()->orders, ['column' => $group, 'direction' => 'asc']);

        // prepare subquery
        $sub_query = $query->toSql();

        // prepare new main base Query\Builder
        $new_base = $this->newQuery()
                         ->from(DB::raw("({$sub_query}) as {$table}"))
                         ->mergeBindings($query->getQuery())
                         ->where($rank_alias, '<=', $number)
                         ->getQuery();

        // replace underlying builder to get rid of previous clauses
        $query->setQuery($new_base);
    }

    /**
     * @param Builder $query
     * @param int     $number
     *
     * @return void
     */
    public function scopeTop(Builder $query, $number = 10): void
    {
        /** @var EloquentModel $this */
        // queried table
        $table = $this->getTable();

        // initialize MySQL variables inline
        $query->from(DB::raw("(SELECT @rank:=0) as vars, {$table}"));

        // if no columns already selected, let's select *
        if (!$query->getQuery()->columns) {
            $query->select("{$table}.*");
        }

        // make sure column aliases are unique
        $rank_alias = 'rank_' . md5(time());

        // apply mysql variables
        $query->addSelect(DB::raw(
            "@rank := @rank+1 as {$rank_alias}"
        ));

        // prepare subquery
        $sub_query = $query->toSql();

        // prepare new main base Query\Builder
        $new_base = $this->newQuery()
                         ->from(DB::raw("({$sub_query}) as {$table}"))
                         ->mergeBindings($query->getQuery())
                         ->where($rank_alias, '<=', $number)
                         ->getQuery();

        // replace underlying builder to get rid of previous clauses
        $query->setQuery($new_base);
    }
}
