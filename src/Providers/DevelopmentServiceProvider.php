<?php

namespace Satanik\Foundation\Providers;

use Illuminate\Support\ServiceProvider;

abstract class DevelopmentServiceProvider extends ServiceProvider
{
    protected $providers = [
    ];
    protected $aliases = [
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        //register the service providers for local environment
        if (!empty($this->providers) && $this->app->isLocal()) {
            foreach ($this->providers as $provider) {
                $this->app->register($provider);
            }
            //register the alias
            if (!empty($this->aliases)) {
                foreach ($this->aliases as $alias => $facade) {
                    $this->app->alias($alias, $facade);
                }
            }
        }

    }
}
