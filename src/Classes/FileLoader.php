<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 04.04.18
 * Time: 11:03
 */

namespace Satanik\Foundation\Classes;


class FileLoader extends \Illuminate\Translation\FileLoader
{
    private $merge_once = false;

    public function mergeOnce()
    {
        $this->merge_once = true;
        return $this;
    }

    /**
     * Add a new namespace to the loader.
     *
     * @param  string $namespace
     * @param  string $hint
     *
     * @return void
     */
    public function addNamespace($namespace, $hint)
    {
        if ($this->merge_once) {
            $this->hints[$namespace] = array_flatten([$this->hints[$namespace] ?? [], $hint]);
        } else {
            parent::addNamespace($namespace, $hint);
        }
        $this->merge_once = false;
    }

    /**
     * Load a namespaced translation group.
     *
     * @param  string $locale
     * @param  string $group
     * @param  string $namespace
     *
     * @return array
     */
    protected function loadNamespaced($locale, $group, $namespace)
    {
        if (isset($this->hints[$namespace])) {
            $paths = $this->hints[$namespace];
            if (!\is_array($paths)) {
                $paths = [$paths];
            }

            $lines = [];
            foreach ($paths as $path) {
                $entries = $this->loadPath($path, $locale, $group);
                foreach ($entries as $key => $entry) {
                    $lines[$key] = $entry;
                }
            }

            return $this->loadNamespaceOverrides($lines, $locale, $group, $namespace);
        }

        return [];
    }


}
