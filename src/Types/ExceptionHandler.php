<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 29.01.18
 * Time: 16:41
 */

namespace Satanik\Foundation\Types;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as BaseHandler;
use Satanik\Exceptions\Concerns\RendersExceptions;

class ExceptionHandler extends BaseHandler
{
    use RendersExceptions;

    /**
     * Report or log an exception.
     *
     * @param  \Exception $e
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function report(Exception $e)
    {
        if (\App::environment() !== 'testing') {
            \DB::rollBack(0);
        }
        return parent::report($e);
    }

    public function render($request, Exception $e)
    {
        if ($response = $this->renderExceptions($request, $e)) {
            return $response;
        }
        return parent::render($request, $e);
    }


}
