<?php

namespace Satanik\Foundation\Reflection;

use Illuminate\Console\DetectsApplicationNamespace;
use Illuminate\Support\Collection;
use Satanik\Foundation\Contracts\ClassLoader as ClassLoaderContract;

final class ClassLoader implements ClassLoaderContract
{
    use DetectsApplicationNamespace;

    private static $instance = null;

    private static function instance(): ClassLoader
    {
        static::$instance = static::$instance ?? new ClassLoader;

        return static::$instance;
    }

    private static function classmap(): array
    {
        $class_map_path = base_path('vendor/composer/autoload_classmap.php');
        /** @noinspection PhpIncludeInspection */
        return include $class_map_path;
    }

    public static function implements(string $interface): Collection
    {
        return collect(static::classmap())->filter(function ($path, $class) use ($interface) {
            if (!starts_with($class, ['Satanik', static::instance()->getAppNamespace()]) && starts_with($path, base_path('vendor'))) {
                return false;
            }

            $r = new \ReflectionClass($class);
            return $r->implementsInterface($interface);
        })->keys();
    }

    public static function inherits(string $parent): Collection
    {
        return collect(static::classmap())->filter(function ($path, $class) use ($parent) {
            if (!starts_with($class, ['Satanik', static::instance()->getAppNamespace()]) && starts_with($path, base_path('vendor'))) {
                return false;
            }

            $r = new \ReflectionClass($class);
            return $r->isSubclassOf($parent);
        })->keys();
    }
}
