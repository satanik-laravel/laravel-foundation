<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 09.11.17
 * Time: 16:43
 */

namespace Satanik\Foundation\Abstraction;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Satanik\Exceptions\Types\Exception;
use Satanik\Exceptions\Types\ValidationException;
use Waavi\Sanitizer\Laravel\SanitizesInput;

abstract class Request extends FormRequest
{
    use SanitizesInput;

    final public function wantsJson(): bool
    {
        return true;
    }

    public function authorize(): bool
    {
        return true;
    }

    public function validateResolved()
    {
        $this->sanitize();
        parent::validateResolved();
    }


    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return void
     *
     * @throws Exception
     */
    protected function failedValidation(Validator $validator): void
    {
        throw new ValidationException($validator);
    }
}
