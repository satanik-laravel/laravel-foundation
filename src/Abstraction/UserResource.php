<?php

namespace Satanik\Foundation\Abstraction;

class UserResource extends Resource
{
    /**
     * Create a new resource instance.
     *
     * @param mixed ...$parameters
     *
     * @return static
     */
    public static function make(...$parameters)
    {
        return app()->make(__CLASS__, $parameters);
    }
}
