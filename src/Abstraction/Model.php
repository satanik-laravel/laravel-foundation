<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 05.11.17
 * Time: 06:08
 */

namespace Satanik\Foundation\Abstraction;

use Satanik\Foundation\Concerns\IsModel;

/**
 * Class Model
 *
 * @mixin \Eloquent
 */
class Model extends \Eloquent
{
    use IsModel;
}
