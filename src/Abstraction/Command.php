<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 19.03.18
 * Time: 15:15
 */

namespace Satanik\Foundation\Abstraction;


class Command
{
    /** @var callable $function */
    private $function;

    /** @var callable $success */
    private $success;

    /** @var callable $fail */
    private $fail;

    public function __construct(callable $function)
    {
        $this->function = $function;
    }

    /**
     * @param callable $function
     * @return Command
     */
    public static function make(callable $function): self
    {
        return new static($function);
    }

    /**
     * @param callable $function
     * @return $this
     */
    public function success(callable $function) : self
    {
        $this->success = $function;
        return $this;
    }

    /**
     * @param callable $function
     * @return $this
     */
    public function fail(callable $function) : self
    {
        $this->fail = $function;
        return $this;
    }

    /**
     *
     */
    public function execute()
    {
        $function = $this->function;
        return $function($this->success, $this->fail);
    }
}
