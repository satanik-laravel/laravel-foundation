<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 14.11.17
 * Time: 10:31
 */

namespace Satanik\Foundation\Abstraction;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends \Illuminate\Routing\Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    final public function transform(array $array)
    {
        return array_map([$this, 'transformObject'], $array);
    }

    protected function transformObject(Model $object)
    {
        return $object;
    }
}
