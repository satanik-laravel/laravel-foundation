<?php

namespace Satanik\Foundation\Abstraction;

use Hash;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use ReflectionObject;
use ReflectionProperty;
use Satanik\Foundation\Concerns\IsModel;
use Satanik\IoC\Concerns\Resolveable;
use Satanik\IoC\Contracts\Resolveable as ResolveableContract;

/**
 * Satanik\Foundation\Abstraction\User
 *
 * @property int $id
 * @property string $class
 * @property int|null $role_id
 * @property string|null $username
 * @property string|null $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $description
 * @property string|null $uid
 * @property string|null $profile_image
 * @property string|null $sector
 * @property string|null $address
 * @property string|null $phone_number
 * @property string|null $fax_number
 * @property mixed|null $services
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Ad[] $ads
 * @property-read false|string $token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Location[] $locations
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Satanik\Payment\Models\Receipt[] $receipts
 * @property-read \Satanik\Roles\Models\Role|null $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\Subscription[] $subscriptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Satanik\Payment\Models\TokenCharging[] $tokenChargings
 * @property-read \Illuminate\Database\Eloquent\Collection|\Satanik\Payment\Models\TokenPurchase[] $tokenPurchases
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User joinModel($model, $one, $operator = null, $two = null, $type = 'inner', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User joinPivot($table)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User nPerGroup($group, $number = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User top($number = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFaxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProfileImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements ResolveableContract
{
    use Notifiable, IsModel, Resolveable;

    protected $fillable = ['email'];
    protected $hidden   = [
        'password',
    ];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     *
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $parent_vars = array_filter(get_class_vars(__CLASS__), function ($prop) {
            return \is_array($prop) && sizeof($prop) > 0 &&
                array_keys($prop) === range(0, count($prop) - 1);
        });
        $reflection  = new ReflectionObject($this);
        $flags       = ReflectionProperty::IS_STATIC;
        $properties  = $reflection->getProperties($flags);
        $properties  = array_map(function (ReflectionProperty $value) {
            return $value->getName();
        }, $properties);

        foreach ($parent_vars as $key => $value) {
            if (!in_array($key, $properties, true)) {
                $this->$key = array_keys(array_merge(array_flip($value), array_flip($this->$key)));
            }
        }
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }
}
