<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 06.12.17
 * Time: 12:35
 */

namespace Satanik\Foundation\Abstraction;


use Iterator;
use ReflectionClass;

/**
 * Class Enum
 */
abstract class Enum implements Iterator
{
    /** @var int __default */
    public const DEFAULT = 0;

    /** @var int $value */
    private $value = 0;

    /** @var int $position */
    private $position = 0;

    /** @var array $values */
    private static $values = [];

    /**
     * Creates a new value of some type
     *
     * @param mixed $initial_value
     */
    public function __construct($initial_value = self::DEFAULT)
    {
        if (empty(self::values())) {
            try {
                $r = new ReflectionClass($this);
                $constants = array_except($r->getConstants(), ['__default']);
                if (\in_array(self::DEFAULT, array_except($constants, ['DEFAULT']), false)) {
                    unset($constants['DEFAULT']);
                }
                self::setValues($constants);
            } catch (\ReflectionException $e) {
                \Log::error($e->getMessage());
            }
        }

        if (\is_int($initial_value)) {
            $this->value = $initial_value;
        } elseif (\is_string($initial_value) && \in_array($initial_value, self::values(), false)) {
            $this->value = self::values()[$initial_value];
        }
    }

    private static function values() {
        if (!isset(self::$values[static::class])) {
            self::$values[static::class] = [];
        }
        return self::$values[static::class];
    }

    private static function setValues($values): void
    {
        if (!isset(self::$values[static::class])) {
            self::$values[static::class] = [];
        }
        self::$values[static::class] = array_merge(static       ::values(),
            \is_array($values) ? $values : [$values]
        );
    }

    /**
     * @param int $initial_value
     *
     * @return self
     */
    final public static function make($initial_value = self::DEFAULT): self
    {
        return new static($initial_value);
    }

    public function toggleFlags(Enum $enum): void
    {
        $length = (int)log($enum->value, 2);
        for ($i = 0; $i < $length + 1; $i++) {
            $flag = $enum->value & (1 << $i);
            if ($flag > 0) {
                $this->value |= $flag;
            }
        }
    }

    public function isset(Enum $enum): bool
    {
        $length = (int)log($enum->value, 2);
        for ($i = 0; $i < $length + 1; $i++) {
            $flag = $enum->value & (1 << $i);
            if ($flag > 0 && ($flag & $this->value) === 0) {
                return false;
            }
        }
        return true;
    }

    public function get()
    {
        return $this->value;
    }

    public function rewind(): void
    {
        $this->position = 0;
    }

    public function current()
    {
        return (static::values()[array_keys(self::values())[$this->position]] & $this->value) > 0;
    }

    public function key()
    {
        return array_keys(self::values())[$this->position];
    }

    public function next(): void
    {
        ++$this->position;
    }

    public function valid(): bool
    {
        return isset(array_keys(self::values())[$this->position]);
    }

    public function options(): array
    {
        return static::values();
    }
}
