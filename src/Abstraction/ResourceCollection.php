<?php

namespace Satanik\Foundation\Abstraction;

use Illuminate\Http\Resources\Json\ResourceCollection as Collection;

class ResourceCollection extends Collection
{
    /** @var array $without_fields */
    protected $without_fields = [];
    /** @var array $only_fields */
    protected $only_fields = [];

    public function __construct($resource, string $resource_type)
    {
        $this->collects = $resource_type;
        parent::__construct($resource);
    }


    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request): array
    {
        return $this->processCollection($request);
    }

    /**
     * @param mixed|array|string $name
     * @param array|null         $fields
     *
     * @return $this
     */
    public function hide($name, ?array $fields = null): self
    {
        if (!$fields) {
            $fields = $name;
            $name = 'self';
        }
        $this->without_fields[$name] = array_unique(
            array_merge($this->without_fields[$name] ?? [], $fields));
        return $this;
    }

    /**
     * @param null $name
     *
     * @return $this
     */
    public function hideTimestamps($name = null): self
    {
        $fields = ['created_at','updated_at'];
        if ($name) {
            return $this->hide($name, $fields);
        }
        return $this->hide($fields);
    }

    /**
     * @param mixed|array|string $name
     * @param array|null         $fields
     *
     * @return $this
     */
    public function only($name, ?array $fields = null): self
    {
        if (!$fields) {
            $fields = $name;
            $name = 'self';
        }
        $this->only_fields[$name] = array_unique(
            array_merge($this->only_fields[$name] ?? [], $fields));
        return $this;
    }

    /**
     * Send fields to hide to UsersResource while processing the collection.
     *
     * @param $request
     * @return array
     */
    protected function processCollection($request): array
    {
        $mapped = $this->collection->map(function (Resource $resource) use ($request) {
            foreach ($this->without_fields as $key => $value) {
                $resource->hide($key, $value);
            }
            foreach ($this->only_fields as $key => $value) {
                $resource->only($key, $value);
            }
            return $resource->toArray($request);
        })->all();
        return $mapped;
    }
}
