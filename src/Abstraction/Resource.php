<?php

namespace Satanik\Foundation\Abstraction;


use Illuminate\Http\Resources\Json\JsonResource;

class Resource extends JsonResource
{
    /** @var array $without_fields */
    protected $without_fields = [];
    /** @var array $only_fields */
    protected $only_fields = [];

    public static function collection($resource)
    {
        return new ResourceCollection($resource, static::class);
    }

    /**
     * @param mixed|array|string $name
     * @param array|null         $fields
     *
     * @return $this
     */
    public function hide($name, ?array $fields = null): self
    {
        if (!$fields) {
            $fields = $name;
            $name   = 'self';
        }
        $this->without_fields[$name] = array_unique(
            array_merge($this->without_fields[$name] ?? [], $fields));
        return $this;
    }

    /**
     * @param null $name
     *
     * @return $this
     */
    public function hideTimestamps($name = null): self
    {
        $fields = ['created_at', 'updated_at'];
        if ($name) {
            return $this->hide($name, $fields);
        }
        return $this->hide($fields);
    }

    /**
     * @param mixed|array|string $name
     * @param array|null         $fields
     *
     * @return $this
     */
    public function only($name, ?array $fields = null): self
    {
        if (!$fields) {
            $fields = $name;
            $name   = 'self';
        }
        $this->only_fields[$name] = array_unique(
            array_merge($this->only_fields[$name] ?? [], $fields));
        return $this;
    }

    /**
     * Remove the filtered keys.
     *
     * @param $array
     *
     * @return array
     */
    protected function filter($array): array
    {
        $fields = collect($array);
        if (!empty($this->without_fields['self'])) {
            $fields = $fields->forget($this->without_fields['self']);
        }
        if (!empty($this->only_fields['self'])) {
            $fields = $fields->only($this->only_fields['self']);
        }

        foreach ($fields->keys() as $key) {
            if (key_exists($key, $this->without_fields)) {
                $fields[$key] = $fields[$key]->hide($this->without_fields[$key]);
            }
            if (key_exists($key, $this->only_fields)) {
                $fields[$key] = $fields[$key]->only($this->only_fields[$key]);
            }
        }

        return $fields->toArray();
    }
}
