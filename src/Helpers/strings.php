<?php

if (!function_exists('transliterate')) {
    /**
     * @param null|string $string
     *
     * @return string
     */
    function transliterate(?string $string): string {
        if (!$string) {
            return '';
        }

        $diacritics = [
            'ä' => 'ae',
            'ö' => 'oe',
            'ü' => 'ue',
            'Ä' => 'Ae',
            'Ö' => 'Oe',
            'Ü' => 'Ue',
            'ß' => 'ss',
        ];
        $string     = str_replace(array_keys($diacritics), array_values($diacritics), $string);
        $string     = iconv("utf-8", "ascii//TRANSLIT//IGNORE", $string);
        return $string;
    }
    \Illuminate\Support\Str::macro('transliterate', 'transliterate');
}
