<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 11.04.18
 * Time: 10:27
 */

if (!function_exists('array_any')) {
    function array_any(array $var, callable $predicate): bool {
        array_walk($var, function(&$element, $key) use ($predicate) {
            $result = $predicate($key, $element);
            assert(is_bool($result));
            $element = (int)$result;
        });

        return array_sum($var) > 0;
    }
}

if (!function_exists('array_all')) {
    function array_all(array $var, callable $predicate): bool {
        array_walk($var, function(&$element, $key) use ($predicate) {
            $result = $predicate($key, $element);
            assert(is_bool($result));
            $element = (int)$result;
        });

        return array_sum($var) == sizeof($var);
    }
}
