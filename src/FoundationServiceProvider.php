<?php

namespace Satanik\Foundation;


use Assert;
use Auth;
use Event;
use Illuminate\Database\Events\TransactionRolledBack;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Validator as CValidator;
use Satanik\Exceptions\Types\Error;
use Satanik\Foundation\Abstraction\User;
use Satanik\Foundation\Abstraction\UserResource;
use Satanik\Foundation\Classes\FileLoader;
use Satanik\Foundation\Contracts\ClassLoader as ClassLoaderContract;
use Satanik\Foundation\Reflection\ClassLoader;
use Satanik\Foundation\Types\ExceptionHandler;
use Validator;


class FoundationServiceProvider extends ServiceProvider
{


    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(): void
    {
        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }

        $this->extendValidator();

        $this->registerEventListener();

        $this->defineAssertMacros();
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/satanik-foundation.php', 'satanik-foundation');

        $this->registerHelper();

        $user_model = config('satanik-foundation.user_model', User::class);

        $this->app->bind(User::class, function ($app, $parameters) use ($user_model) {
            Assert::classExists($user_model);

            if ($parameters['class'] ?? false) {
                return $user_model;
            }
            return new $user_model;
        });

        $user_resource = config('satanik-foundation.user_resource', UserResource::class);

        $this->app->bind(UserResource::class, function ($app, $parameters) use ($user_resource) {
            Assert::classExists($user_resource);
            Assert::inherits($user_resource, Resource::class);

            if ($parameters['class'] ?? false) {
                return $user_resource;
            }

            if (sizeof($parameters) === 0) {
                $parameters[] = User::resolve();
            }
            return new $user_resource(...$parameters);
        });

        $this->app->extend('translation.loader', function ($command, $app) {
            return new FileLoader($app['files'], $app['path.lang']);
        });

        $this->app->bindIf(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            ExceptionHandler::class,
            true
        );

        $this->app->bind(ClassLoaderContract::class, ClassLoader::class);
        $this->app->alias(ClassLoaderContract::class, 'satanik-classloader');
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole(): void
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/satanik-foundation.php' => config_path('satanik-foundation.php'),
        ], 'satanik.foundation.config');
    }

    private function extendValidator(): void
    {
        $occurance = function ($attribute, $parameters, CValidator $validator) {
            /**
             * [$exists] tells if the attribute is present inside the data at the path location
             * [$array]  array to save recursive access to attribute e.g. attr.0.attr2
             *           add all data first
             * [$keys]   afterwards the path to the attribute to test is written in dot-notation and is exploded
             * [$i]      level of the nested array
             * [while]   walk only as deep as there are keys
             *           test if a key exists at the path location and AND it with $exists
             * [body]    add the next nested array to $array for later access
             */
            $exists = true;
            $array  = [$validator->getData()];
            $keys   = explode('.', $attribute);
            $i      = 0;
            while ($i < sizeof($keys) && ($exists &= isset($array[$i][$keys[$i]]))) {
                $array[] = $array[$i][$keys[$i]];
                $i++;
            }

            /**
             * [$no_other] tells if the other $parameters are not available at the same object
             * [$i]        index of the parameter in $parameters to test
             * [while]     iterate only as deep as there are parameters
             *             test if a parameter is absent and AND it with $no_other
             */
            $other = [];
            /** @noinspection PhpStatementHasEmptyBodyInspection */
            for ($i = 0; $i < sizeof($parameters); $i++) {
                $other[$parameters[$i]] = !empty($array[sizeof($keys) - 1][$parameters[$i]]);
            }

            return [$exists, $other];
        };

        Validator::extend('forbidden_with_any',
            function ($attribute, $values, $parameters, CValidator $validator) use ($occurance) {
                [$exists, $other] = $occurance($attribute, $parameters, $validator);
                $any_other = array_reduce($other, function ($carry, $item) {
                    return $carry ? $carry | $item : $item;
                });

                return !($exists && $any_other);

                /**
                 * material implication
                 * 1 1 = 1
                 * 1 0 = 0
                 * 0 1 = 1
                 * 0 0 = 1
                 */
                return (!$exists && !$any_other) || $any_other;
            }, 'The field [:field] may only be present if none of the fields [:values] are present');

        Validator::extend('forbidden_with_all',
            function ($attribute, $values, $parameters, CValidator $validator) use ($occurance) {
                [$exists, $other] = $occurance($attribute, $parameters, $validator);
                $all_other = array_reduce($other, function ($carry, $item) {
                    return $carry ? $carry & $item : $item;
                });

                return !($exists && $all_other);
            }, 'The field [:field] may only be present if not all of the fields [:values] are present');

        Validator::extend('forbidden_without_any',
            function ($attribute, $values, $parameters, CValidator $validator) use ($occurance) {
                [$exists, $other] = $occurance($attribute, $parameters, $validator);
                $any_other = array_reduce($other, function ($carry, $item) {
                    return $carry ? $carry | $item : $item;
                });

                /**
                 * material implication
                 * 1 1 = 1
                 * 1 0 = 0
                 * 0 1 = 1
                 * 0 0 = 1
                 */
                return (!$exists && !$any_other) || $any_other;
            }, 'The field [:field] may only be present if at least one of the fields [:values] are present');

        Validator::extend('forbidden_without_all',
            function ($attribute, $values, $parameters, CValidator $validator) use ($occurance) {
                [$exists, $other] = $occurance($attribute, $parameters, $validator);
                $any_other = array_reduce($other, function ($carry, $item) {
                    return $carry ? $carry & $item : $item;
                });

                /**
                 * material implication
                 * 1 1 = 1
                 * 1 0 = 0
                 * 0 1 = 1
                 * 0 0 = 1
                 */
                return (!$exists && !$any_other) || $any_other;
            }, 'The field [:field] may only be present if all of the fields [:values] are present');

        foreach (['forbidden_with_any', 'forbidden_with_all', 'forbidden_without_any', 'forbidden_without_all'] as $key) {
            Validator::replacer($key, function ($message, $attribute, $rule, $parameters) {
                return str_replace([':field', ':values'], [$attribute, implode(', ', $parameters)], $message);
            });
        }

        Validator::extend('forbidden_without_all', function ($attribute, $values, $parameters, CValidator $validator) {
            /**
             * [$exists] tells if the attribute is present inside the data at the path location
             * [$array]  array to save recursive access to attribute e.g. attr.0.attr2
             *           add all data first
             * [$keys]   afterwards the path to the attribute to test is written in dot-notation and is exploded
             * [$i]      level of the nested array
             * [while]   walk only as deep as there are keys
             *           test if a key exists at the path location and AND it with $exists
             * [body]    add the next nested array to $array for later access
             */
            $exists = true;
            $array  = [$validator->getData()];
            $keys   = explode('.', $attribute);
            $i      = 0;
            while ($i < sizeof($keys) && ($exists &= isset($array[$i][$keys[$i]]))) {
                $array[] = $array[$i][$keys[$i]];
                $i++;
            }
            /**
             * [$other] tells if the other $parameters are available at the same object
             * [$i]     index of the parameter in $parameters to test
             * [while]  iterate only as deep as there are parameters
             *          test if a parameter is absent and AND it with $no_other
             */
            $other = true;
            $i     = 0;
            /** @noinspection PhpStatementHasEmptyBodyInspection */
            while ($i < sizeof($parameters) && ($other &= !empty($array[sizeof($keys) - 1][$parameters[$i]]))) {
                $i++;
            }

            /**
             * material implication
             * 1 1 = 1
             * 1 0 = 0
             * 0 1 = 1
             * 0 0 = 1
             */
            return (!$exists && !$other) || $other;
        }, 'The field [:field] may only be available if all of the fields [:values] are present');

        Validator::replacer('forbidden_without_all', function ($message, $attribute, $rule, $parameters) {
            return str_replace([':field', ':values'], [$attribute, implode(', ', $parameters)], $message);
        });

        Validator::extend('instance_of', function ($attribute, $values, $parameters, CValidator $validator) {
            foreach ($parameters as $parameter) {
                $check = $validator->getData()[$attribute] instanceof $parameter;
                $check |= is_subclass_of($validator->getData()[$attribute], $parameter);
                $check |= in_array($parameter, class_uses($validator->getData()[$attribute]), true);
                if (!$check) {
                    return false;
                }
            }
            return true;
        });

        Validator::replacer('instance_of', function ($message, $attribute, $rule, $parameters) {
            return str_replace([':object', ':classes'], [$attribute, implode(', ', $parameters)], $message);
        });
    }

    public function registerEventListener(): void
    {
        Event::listen(TransactionRolledBack::class, function (TransactionRolledBack $event) {
            if (\DB::transactionLevel() === 0) {
                $connection = $event->connection;
                $tables     = $connection->select('SHOW FULL TABLES WHERE Table_Type = "BASE TABLE"');
                $column     = "Tables_in_{$connection->getDatabaseName()}";
                foreach ($tables as $table) {
                    $connection->statement("ALTER TABLE {$table->$column} AUTO_INCREMENT = 0");
                }
            }
        });
    }

    private function defineAssertMacros(): void
    {
        /**
         * @param \Satanik\Authentication\Models\User $user
         * @param callable                            $predicate
         *
         * @throws \Satanik\Exceptions\Types\Exception
         */
        Assert::macro('authorized', function (User $user, callable $predicate): void {
            $result = $predicate($user);
            Assert::exception(
                is_bool($result) && $result,
                Error::USER_NOT_AUTHORIZED,
                ['user' => $user->email ?? ''],
                403
            );
        });

        /**
         * @param array $credentials
         *
         * @return \Satanik\Authentication\Models\User
         * @throws \Satanik\Exceptions\Types\Exception
         */
        Assert::macro('login', function (array $credentials): User {
            $succeeded = Auth::attempt($credentials);
            Assert::exception($succeeded, Error::LOGIN_FAILED, $credentials);
            return Auth::user();
        });
    }


    private function registerHelper(): void
    {
        foreach (glob(__DIR__ . '/Helpers/*.php') as $filename) {
            require_once $filename;
        }
    }
}
