<?php

namespace Satanik\Foundation\Facades;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Facade;
use Satanik\Foundation\Contracts\ClassLoader as ClassLoaderContract;

class ClassLoader extends Facade implements ClassLoaderContract
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor(): string
    {
        return 'satanik-classloader';
    }

    /**
     * @static
     * @param string $interface
     *
     * @return \Illuminate\Support\Collection
     */
    public static function implements(string $interface): Collection
    {
        return \Satanik\Foundation\Reflection\ClassLoader::implements($interface);
    }

    /**
     * @static
     * @param string $parent
     *
     * @return \Illuminate\Support\Collection
     */
    public static function inherits(string $parent): Collection
    {
        return \Satanik\Foundation\Reflection\ClassLoader::inherits($parent);
    }
}
